DESCRIPTION = "Graphic installer live image."

inherit redfield-image

SRC_URI = "file://${FILE_DIRNAME}/installer-image.wks"
WKS_FILE_DEPENDS = "syslinux syslinux-native dosfstools-native mtools-native gptfdisk-native"
IMAGE_FSTYPES += " wic "
WKS_FILE = "installer-image"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"
INITRD_IMAGE_LIVE = "installer-initramfs"

IMAGE_INSTALL += " \
    adwaita-icon-theme-cursors \
    adwaita-icon-theme-hires \
    coreutils \
    dash-to-dock \
    dosfstools \
    e2fsprogs-mke2fs \
    flat-remix-gnome \
    gdm \
    gnome-session \
    gnome-settings-daemon \
    gnome-session \
    gnome-shell-extensions \
    gnome-tweaks \
    go-installer \
    installer-gnome-configs \
    installer-iptables-configs \
    installer-lvm-configs \
    installer-netctl-configs \
    installer-networkd-configs \
    installer-sysctl-configs \
    kernel-modules \
    linux-firmware \
    lvm2 \
    packagegroup-core-boot \
    packagegroup-fonts-truetype-core \
    packagegroup-gnome3-base \
    packagegroup-network-driver-modules \
    packagegroup-networkvm \
    packagegroup-storage-driver-modules \
    parted \
    pciutils \
    plymouth \
    redfield-image-configs \
    sb-preloader \
    usbutils \
    wifimenu \
    wireless-regdb-static \
    wpa-supplicant \
    xterm \
    ykpers \
    yubikey-full-disk-encryption \
"

LICENSE = "MIT"

image_postprocess_storage_point() {
    mkdir -p ${IMAGE_ROOTFS}/storage
}

IMAGE_ROOTFS_EXTRA_SPACE += " + 32768 "

ROOTFS_POSTPROCESS_COMMAND_append = " \
    image_postprocess_storage_point; \
"
