DESCRIPTION = "DOM0 NETWORKD CONFIGS"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://10-brmgmt.netdev \
    file://10-outside.network \
    file://11-brmgmt.network \
"

do_install() {
    install -d ${D}${nonarch_libdir}/systemd/network
    install -m 0644 ${WORKDIR}/10-brmgmt.netdev ${D}${nonarch_libdir}/systemd/network
    install -m 0644 ${WORKDIR}/10-outside.network ${D}${nonarch_libdir}/systemd/network
    install -m 0644 ${WORKDIR}/11-brmgmt.network ${D}${nonarch_libdir}/systemd/network
}

RDEPENDS_${PN} = " \
    bash \
"

FILES_${PN} += "${nonarch_libdir}/systemd"
