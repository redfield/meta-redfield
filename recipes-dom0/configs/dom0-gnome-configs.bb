DESCRIPTION = "Dom0 GNOME Shell Configurations"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://dconf.d/dconf-user.conf \
    file://dconf-profile \
    file://background.png \
    file://windows.png \
    file://redfield-icon-default.png \
    file://redfield-session.desktop \
    file://xterm.desktop \
    file://redctl-gui.desktop \
    file://redctl-new-vm.png \
    file://disable-tty.conf \
"

DEPENDS += " dconf-native "

FILES_${PN} += "${sysconfdir}/dconf/db/local"
FILES_${PN} += "${sysconfdir}/xdg/autostart/redfield-session.desktop"
FILES_${PN} += "${datadir}/redfield"
FILES_${PN} += "${datadir}/X11/xorg.conf.d/20-disable-tty.conf"
FILES_${PN} += "${datadir}/applications/xterm.desktop"
FILES_${PN} += "${datadir}/applications/redctl-gui.desktop"

do_compile() {
    ${RECIPE_SYSROOT_NATIVE}/${bindir}/dconf compile ${WORKDIR}/local ${WORKDIR}/dconf.d
}

do_install() {
    install -d ${D}/${datadir}/redfield
    install -d ${D}/${datadir}/redfield/icons
    install -d ${D}/${datadir}/redfield/system-icons
    install -d ${D}/${datadir}/redfield/backgrounds

    install -d ${D}/${datadir}/applications
    install -d ${D}/${sysconfdir}/dconf/db
    install -d ${D}/${sysconfdir}/dconf/profile
    install -d ${D}/${sysconfdir}/xdg/autostart
    install -d ${D}/${datadir}/X11/xorg.conf.d

    install -m 0644 ${WORKDIR}/local ${D}/${sysconfdir}/dconf/db/local
    install -m 0644 ${WORKDIR}/dconf-profile ${D}/${sysconfdir}/dconf/profile/local

    install -m 0644 ${WORKDIR}/xterm.desktop ${D}/${datadir}/applications/xterm.desktop
    install -m 0644 ${WORKDIR}/redctl-gui.desktop ${D}/${datadir}/applications/redctl-gui.desktop

    install -m 0644 ${WORKDIR}/background.png ${D}/${datadir}/redfield/backgrounds/background.png
    install -m 0644 ${WORKDIR}/windows.png ${D}/${datadir}/redfield/icons/windows.png
    install -m 0644 ${WORKDIR}/redfield-icon-default.png ${D}/${datadir}/redfield/icons/redfield-icon-default.png
    install -m 0644 ${WORKDIR}/redctl-new-vm.png ${D}/${datadir}/redfield/system-icons/redctl-new-vm.png

    install -m 0644 ${WORKDIR}/redfield-session.desktop ${D}/${sysconfdir}/xdg/autostart/redfield-session.desktop
    install -m 0644 ${WORKDIR}/disable-tty.conf ${D}/${datadir}/X11/xorg.conf.d/20-disable-tty.conf
}

