DESCRIPTION = "DOM0 QEMU CONFIGS"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

SRC_URI = " \
    file://vif-emu.rules \
    file://qemu-ifup \
    file://qemu-ifdown \
"

do_install() {
    install -d ${D}${sysconfdir}/xen
    install -d ${D}${sysconfdir}/xen/scripts
    install -d ${D}${sysconfdir}/udev
    install -d ${D}${sysconfdir}/udev/rules.d
    install -m 0644 ${WORKDIR}/vif-emu.rules ${D}${sysconfdir}/udev/rules.d/vif-emu.rules
    install -m 0755 ${WORKDIR}/qemu-ifup ${D}${sysconfdir}/xen/scripts/qemu-ifup
    install -m 0755 ${WORKDIR}/qemu-ifdown ${D}${sysconfdir}/xen/scripts/qemu-ifdown
}

RDEPENDS_${PN} = " \
    bash \
"
