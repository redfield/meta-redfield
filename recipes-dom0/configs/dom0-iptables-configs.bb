DESCRIPTION = "DOM0 IPTABLES CONFIGS"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://ip6tables.rules \
    file://iptables.rules \
"

do_install() {
    install -d ${D}${sysconfdir}
    install -m 0644 ${WORKDIR}/ip6tables.rules ${D}${sysconfdir}
    install -m 0644 ${WORKDIR}/iptables.rules ${D}${sysconfdir}
}

RDEPENDS_${PN} = " \
    iptables \
"
