DESCRIPTION = "PRIME DOM0 IMAGE"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit redfield-image

do_bootimg[depends] += "dom0-image-initramfs:do_image_complete"

# We should move these arch includes to the meta-virt upstream layer

IMAGE_INSTALL += " \
    ${@bb.utils.contains('ARCH', 'x86', 'ovmf', '', d)} \
    ${@bb.utils.contains('ARCH', 'x86', 'qemu', '', d)} \
    ${@bb.utils.contains('ARCH', 'x86', 'seabios', '', d)} \
    ${@bb.utils.contains('ARCH', 'x86', 'vgabios', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'redfield-audio', 'packagegroup-redfield-audio', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'redfield-storage', 'packagegroup-redfield-storage', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'redfield-gui', 'packagegroup-redfield-gui', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'redfield-network', 'packagegroup-redfield-network', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'redfield-usb', 'packagegroup-redfield-usb', '', d)} \
    ${@bb.utils.contains('MACHINE_FEATURES', 'pci', 'pciutils', '', d)} \
    attach-aircard-ndvm \
    argo-module \
    automount-usb \
    coreutils \
    dom0-efi-config \
    dom0-modules-configs \
    dom0-qemu-configs \
    dom0-sshd-configs \
    dom0-sysctl-configs \
    dom0-udev-configs \
    dosfstools \
    exfat-utils \
    fuse-exfat \
    e2fsprogs-mke2fs \
    evtest \
    go-netctl \
    go-netctl-back \
    go-redctl \
    go-redctl-gui \
    go-installer \
    grub-bootconf \
    grub-efi \
    kernel-image-bzimage \
    kernel-module-xen-blkback \
    kernel-module-xen-netback \
    kernel-module-xen-netfront \
    kernel-modules \
    ndvm-create \
    ntfs-3g-ntfsprogs \
    plymouth \
    redfield-image-configs \
    systemd-container \
    xen-tools \
    xen-efi \
    xen \
    xen-tools-xenstat \
"

image_postprocess_dom0() {
    install -m 0644 ${DEPLOY_DIR_IMAGE}/dom0-image-initramfs-${MACHINE}.cpio.gz ${IMAGE_ROOTFS}/boot/initramfs.gz
    rm ${IMAGE_ROOTFS}/${systemd_unitdir}/system/xendomains.service
    mv ${IMAGE_ROOTFS}${sysconfdir}/ssh/sshd_config_redfield ${IMAGE_ROOTFS}${sysconfdir}/ssh/sshd_config
    rm ${IMAGE_ROOTFS}${sysconfdir}/ssh/sshd_config_readonly

    ##################################################
    # read-write, persistent config symlinks
    ##################################################
    ln -sf /storage/etc/xorg.conf ${IMAGE_ROOTFS}/etc/xorg.conf
    rmdir ${IMAGE_ROOTFS}/etc/systemd/network
    ln -sf /storage/etc/network ${IMAGE_ROOTFS}/etc/systemd/network
}

image_postprocess_dom0_configure_journald() {
    sed -i -e 's/.*ForwardToSyslog.*/ForwardToSyslog=no/' ${IMAGE_ROOTFS}${sysconfdir}/systemd/journald.conf
    sed -i -e 's/.*ForwardToConsole.*/ForwardToConsole=yes/' ${IMAGE_ROOTFS}${sysconfdir}/systemd/journald.conf
}

image_postprocess_dom0_disable_suspend() {
    sed -i -e 's/.*HandleSuspendKey.*/HandleSuspendKey=ignore/' ${IMAGE_ROOTFS}${sysconfdir}/systemd/logind.conf
    sed -i -e 's/.*HandleHibernateKey.*/HandleHibernateKey=ignore/' ${IMAGE_ROOTFS}${sysconfdir}/systemd/logind.conf
    sed -i -e 's/.*HandleLidSwitch.*/HandleLidSwitch=ignore/' ${IMAGE_ROOTFS}${sysconfdir}/systemd/logind.conf
    sed -i -e 's/.*HandleLidSwitchExternalPower.*/HandleLidSwitchExternalPower=ignore/' ${IMAGE_ROOTFS}${sysconfdir}/systemd/logind.conf
}

image_postprocess_dom0_enable_tty4() {
    systemctl --root ${IMAGE_ROOTFS} enable getty@tty4.service
}

IMAGE_ROOTFS_EXTRA_SPACE += " + 32768 "

ROOTFS_POSTPROCESS_COMMAND_append = " \
    image_postprocess_tmpfs_etc_lvm; \
    image_postprocess_tmpfs_media; \
    image_postprocess_dom0; \
    image_postprocess_dom0_configure_journald; \
    image_postprocess_dom0_disable_suspend; \
    image_postprocess_dom0_enable_tty4; \
    image_postprocess_purge_default_certificates; \
"

ROOTFS_POSTPROCESS_COMMAND_remove = " \
    ssh_allow_empty_password; \
    read_only_sshd; \
"
