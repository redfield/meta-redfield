do_install[depends] += "${BPN}-image:do_image_complete"

DEST_IMAGE_PATH ?= "/images/"
DEST_IMAGE_EXTENSION ?= "ext4"
DEST_FILE_NAME ?= "${BPN}.raw"

SRC_URI = " \
    file://${BPN}.yaml \
"

do_install() {
    mkdir -p ${D}/images
    mkdir -p ${D}/${sysconfdir}/installer.d

    install -m 0644 ${DEPLOY_DIR_IMAGE}/${BPN}-image-${MACHINE}.${DEST_IMAGE_EXTENSION} ${D}${DEST_IMAGE_PATH}${DEST_FILE_NAME}
    gzip ${D}${DEST_IMAGE_PATH}${DEST_FILE_NAME}
    install -m 0644 ${WORKDIR}/${BPN}.yaml ${D}/${sysconfdir}/installer.d/${BPN}.yaml
}

FILES_${PN} = " \
    ${DEST_IMAGE_PATH}${DEST_FILE_NAME}.gz \
    ${sysconfdir}/installer.d/${BPN}.yaml \
"
INSANE_SKIP_${PN} += " file-rdeps "
