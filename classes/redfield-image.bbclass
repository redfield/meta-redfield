IMAGE_FSTYPES = "ext4"

inherit image

IMAGE_LINGUAS = "en-us"

IMAGE_INSTALL = "\
    base-files \
    base-passwd \
    bind-utils \
    coreutils \
    gawk \
    grep \
    iproute2 \
    iputils \
    iputils-arping \
    iputils-clockdiff \
    iputils-ping \
    iputils-tracepath \
    iputils-traceroute6 \
    nano \
    netbase \
    net-tools \
    rng-tools \
    sed \
    shadow \
    systemd \
    systemd-compat-units \
    vim-tiny \
    ${MACHINE_ESSENTIAL_EXTRA_RDEPENDS} \
"

IMAGE_FEATURES = " \
    read-only-rootfs \
    empty-root-password \
    allow-empty-password \
"

ROOTFS_RO_UNNEEDED = "update-rc.d ${VIRTUAL-RUNTIME_update-alternatives} ${ROOTFS_BOOTSTRAP_INSTALL}"

#########################
# DEFAULT POSTPROCESS FUNCTIONS
#########################

image_postprocess_symlink_usr_ld_linux() {
    # XXX: required by iproute2? must be a bug somewhere...
    ln -sf /lib/ld-linux-x86-64.so.2 ${IMAGE_ROOTFS}/usr/lib/ld-linux-x86-64.so.2
}

fakeroot python image_postprocess_default_image_timestamp() {
    from oe.rootfs import image_list_installed_packages
    from oe.utils import format_pkg_list

    dest_dir = d.getVar('IMAGE_ROOTFS')
    image_base_name = d.getVar('IMAGE_BASENAME')
    image_date = d.getVar('IMAGE_VERSION_SUFFIX')

    if not dest_dir:
        return

    pkgs = image_list_installed_packages(d)
    with open(dest_dir + '/etc/redfield-version', 'w+') as image_manifest:
        image_manifest.write(image_base_name + '\n')
        image_manifest.write(image_date + '\n')
        image_manifest.write('--------------------------------\n')
        image_manifest.write(format_pkg_list(pkgs, "ver"))

}

image_postprocess_default_image_hostname() {
    IMG=$(echo "${IMAGE_BASENAME}" | cut -f 1 -d '-')
    echo "redfield-${IMG}" > ${IMAGE_ROOTFS}/etc/hostname
    echo "local" > ${IMAGE_ROOTFS}/etc/domainname
}

image_postprocess_override_network_online_target_timeout() {
    mkdir -p ${IMAGE_ROOTFS}/etc/systemd/system/systemd-networkd-wait-online.service.d
    echo "[Service]" > ${IMAGE_ROOTFS}/etc/systemd/system/systemd-networkd-wait-online.service.d/override.conf
    echo "ExecStart=" >> ${IMAGE_ROOTFS}/etc/systemd/system/systemd-networkd-wait-online.service.d/override.conf
    echo "ExecStart=/lib/systemd/systemd-networkd-wait-online --timeout=5" >> ${IMAGE_ROOTFS}/etc/systemd/system/systemd-networkd-wait-online.service.d/override.conf
}

image_postprocess_override_systemd_timeout() {
    sed -i -e 's/.*DefaultTimeoutStopSec.*/DefaultTimeoutStopSec=10s/' ${IMAGE_ROOTFS}${sysconfdir}/systemd/system.conf
}

ROOTFS_POSTPROCESS_COMMAND_append = " \
    image_postprocess_symlink_usr_ld_linux; \
    image_postprocess_default_image_hostname; \
    image_postprocess_override_network_online_target_timeout; \
"

#########################
# SHARED (SELECTABLE) POSTPROCESS FUNCTIONS
#########################
image_postprocess_tmpfs_etc_lvm() {
    echo "tmpfs /etc/lvm tmpfs defaults 0  0" >> ${IMAGE_ROOTFS}/etc/fstab
}

image_postprocess_tmpfs_root_home() {
    echo "tmpfs /home/root tmpfs defaults 0  0" >> ${IMAGE_ROOTFS}/etc/fstab
}

# Useful for auto-mounting USB
image_postprocess_tmpfs_media() {
    echo "tmpfs /media tmpfs defaults 0  0" >> ${IMAGE_ROOTFS}/etc/fstab
}

image_postprocess_divert_iptables() {
    mv ${IMAGE_ROOTFS}/usr/sbin/iptables ${IMAGE_ROOTFS}/usr/sbin/iptables.disabled
}

image_postprocess_purge_default_certificates() {
    rm -rf ${IMAGE_ROOTFS}/usr/share/ca-certificates/*
    rm -rf ${IMAGE_ROOTFS}/etc/ssl/certs/*
}


#########################
# DEBUG LOVE
#########################

IMAGE_FEATURES += " \
    debug-tweaks \
    package-management \
"

IMAGE_INSTALL += " \
    binutils \
    ca-certificates \
    curl \
    file \
    findutils \
    gdb \
    gzip \
    ldd \
    less \
    lsof \
    openssh \
    openssl \
    opkg \
    opkg-utils \
    strace \
    tcpdump \
    pciutils \
    procps \
    psmisc \
    rsync \
    systemd-analyze \
    tar \
    usbutils \
    wget \
    which \
    zlib \
    xz \
"

sshd_permit_empty_password() {
    sed -i 's|#PermitEmptyPasswords no|PermitEmptyPasswords yes|g' ${IMAGE_ROOTFS}${sysconfdir}/ssh/sshd_config
}

ROOTFS_POSTPROCESS_COMMAND_append = " \
    sshd_permit_empty_password; \
    image_postprocess_default_image_timestamp; \
"

# set default efi provider
EFI_PROVIDER = "systemd-boot"

# efi only images by default
PCBIOS = "0"
EFI = "1"

SYSTEMD_BOOT_TIMEOUT = "0"

IMAGE_ROOTFS_SIZE ?= "8192"
IMAGE_ROOTFS_EXTRA_SPACE_append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "" ,d)}"
