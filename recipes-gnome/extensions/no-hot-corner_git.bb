DESCRIPTION = "No top-left hot corner extension"
LICENSE = "GPLv2"
DEPENDS = " \
    gnome-shell \
    gsettings-desktop-schemas \
    glib-2.0-native \
"

LIC_FILES_CHKSUM = "file://LICENSE;md5=b974eace4083542f35da69980c28594b"

SRCREV = "6f47dd58796034f38e7aface577f6a76301c4a3b"

SRC_URI = "git://github.com/HROMANO/nohotcorner;protocol=http;branch=master"

S = "${WORKDIR}/git"

FILES_${PN} += "${datadir}"

do_install() {
    MODULE="nohotcorner@azuri.free.fr"
    DESTDIR="${D}/${datadir}/gnome-shell/extensions/${MODULE}"

    install -d ${DESTDIR}
    install -m 0644 ${S}/* ${DESTDIR}/
}
