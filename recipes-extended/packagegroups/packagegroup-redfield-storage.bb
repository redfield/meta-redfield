SUMMARY = "Desirable packages for redfield storage support"
LICENSE = "MIT"

inherit packagegroup

RDEPENDS_${PN} = " \
    efibootmgr \
    lvm2 \
    ykpers \
    yubikey-full-disk-encryption \
"
