SUMMARY = "network hardware drivers"
LICENSE = "MIT"

inherit packagegroup

RDEPENDS_${PN} = "\
    crda \
    wireless-regdb-static \
    kernel-module-8139cp \
    kernel-module-8139too \
    kernel-module-ath \
    kernel-module-ath5k \
    kernel-module-ath9k \
    kernel-module-ath9k-common \
    kernel-module-ath9k-hw \
    kernel-module-ath10k-core \
    kernel-module-ath10k-pci \
    kernel-module-b44 \
    kernel-module-bcm7xxx \
    kernel-module-bcma \
    kernel-module-bcm-phy-lib \
    kernel-module-bnx2 \
    kernel-module-bnx2x \
    kernel-module-bnxt-en \
    kernel-module-bonding \
    kernel-module-brcmfmac \
    kernel-module-brcmsmac \
    kernel-module-brcmutil \
    kernel-module-ccm \
    kernel-module-cfg80211 \
    kernel-module-crc32-generic \
    kernel-module-crc8 \
    kernel-module-e1000 \
    kernel-module-e1000e \
    kernel-module-e100 \
    kernel-module-iwlwifi \
    kernel-module-iwlmvm \
    kernel-module-ixgb \
    kernel-module-ixgbe \
    kernel-module-mac80211 \
    kernel-module-r8169 \
    kernel-module-romfs \
    kernel-module-r8169 \
    kernel-module-rtl8188ee \
    kernel-module-rtl8192c-common \
    kernel-module-rtl8192ce \
    kernel-module-rtl8192de \
    kernel-module-rtl8192ee \
    kernel-module-rtl8192se \
    kernel-module-rtl8723ae \
    kernel-module-rtl8723be \
    kernel-module-rtl8723-common \
    kernel-module-rtl8821ae \
    kernel-module-rtl-pci \
    kernel-module-rtlwifi \
    kernel-module-tg3 \
"
