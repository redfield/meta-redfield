SUMMARY = "Desirable packages for redfield network support"
LICENSE = "MIT"

inherit packagegroup

RDEPENDS_${PN} = " \
    ${@bb.utils.contains('DISTRO_FEATURES', 'redfield-gui', 'wifimenu', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'redfield-gui', 'webkitgtk', '', d)} \
    dom0-iptables-configs \
    dom0-networkd-configs \
"
