From ee849bfbc74dfdb40d24998608425e0a49b7c21c Mon Sep 17 00:00:00 2001
From: Chris Patterson <pattersonc@ainfosec.com>
Date: Wed, 3 Apr 2019 16:15:34 -0400
Subject: [PATCH 2/2] libxl_dm: honor opengl flag for SDL

It was honored in libxl__build_device_model_args_old(), but did not
transition to libxl__build_device_model_args_new().

The opengl flag is useful for SDL to toggle virgl when using virtio-vga.

To accomplish this, we also switch from the short-hand "-sdl" notation to
"-display sdl", which should be fine compatibility-wise as "-display sdl"
is used as far back as qemu 0.15 [1].

"-display sdl,gl=on" is supported as far back as qemu 2.7 [2].

[1] https://github.com/qemu/qemu/blob/stable-0.15/qemu-options.hx#L617
[2] https://github.com/qemu/qemu/blob/stable-2.7/qemu-options.hx#L929

Signed-off-by: Chris Patterson <pattersonc@ainfosec.com>

Update for Xen 4.14.0.

Signed-off-by: Nick Rosbrook <rosbrookn@ainfosec.com>
---
 docs/man/xl.cfg.5.pod.in | 3 +--
 tools/libxl/libxl_dm.c   | 5 ++++-
 2 files changed, 5 insertions(+), 3 deletions(-)

diff --git a/docs/man/xl.cfg.5.pod.in b/docs/man/xl.cfg.5.pod.in
index 0d4fd49be4..a2d3988214 100644
--- a/docs/man/xl.cfg.5.pod.in
+++ b/docs/man/xl.cfg.5.pod.in
@@ -2297,8 +2297,7 @@ Simple DirectMedia Layer). The default is (0) not enabled.
 
 =item B<opengl=BOOLEAN>
 
-Enable OpenGL acceleration of the SDL display. Only effects machines
-using B<device_model_version="qemu-xen-traditional"> and only if the
+Enable OpenGL acceleration of the SDL display.  Only works if the
 device-model was compiled with OpenGL support. Default is (0) false.
 
 =item B<nographic=BOOLEAN>
diff --git a/tools/libxl/libxl_dm.c b/tools/libxl/libxl_dm.c
index f2dc5696b9..0f15e5c9f0 100644
--- a/tools/libxl/libxl_dm.c
+++ b/tools/libxl/libxl_dm.c
@@ -1334,7 +1334,10 @@ static int libxl__build_device_model_args_new(libxl__gc *gc,
     flexarray_append_pair(dm_args, "-display", "none");
 
     if (sdl && !is_stubdom) {
-        flexarray_append(dm_args, "-sdl");
+        flexarray_append_pair(dm_args, "-display", GCSPRINTF("sdl%s",
+                              libxl_defbool_val(sdl->opengl) ?
+                              ",gl=on" : ""));
+
         if (sdl->display)
             flexarray_append_pair(dm_envs, "DISPLAY", sdl->display);
         if (sdl->xauthority)
-- 
2.20.1

