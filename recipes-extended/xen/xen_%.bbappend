FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += " \
    file://defconfig \
    file://xendriverdomain-hack.patch \
    file://0001-libxl-Add-virtio-vga-interface-support-for-qemu.patch \
    file://0002-libxl_dm-honor-opengl-flag-for-SDL.patch \
"

INHIBIT_SYSROOT_STRIP = "1"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
INHIBIT_PACKAGE_STRIP = "1"
INSANE_SKIP_${PN} = "already-stripped"
INSANE_SKIP_${PN}-hvmloader = "arch"
INSANE_SKIP_${PN}-hvmloader += " already-stripped "
