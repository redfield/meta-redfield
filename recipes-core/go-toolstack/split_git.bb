SUMMARY = "Redfield split protocol buffer definitions"
HOMEPAGE = "http://www.gitlab.com/redfield/split"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${S}/src/${GO_IMPORT}/LICENSE;md5=307f9948f6b9a9c566f7a86c108934b3"

GO_IMPORT = "gitlab.com/redfield/split"

SRC_URI = " \
    git://${REDFIELD_SOURCES_URL}/split.git;protocol=${REDFIELD_SOURCES_PROTOCOL};branch=${REDFIELD_BRANCH};destsuffix=${BPN}-${PV}/src/${GO_IMPORT} \
"

SRCREV = "${AUTOREV}"

do_install() {
    oe_runmake -C src/${GO_IMPORT} install-proto includedir=${D}/${includedir}
}

FILES_${PN} = "${includedir}"

DEPENDS_${PN}-dev = "bash make"
