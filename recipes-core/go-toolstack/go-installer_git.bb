SUMMARY = "Redfield Installer"
HOMEPAGE = "http://www.gitlab.com/redfield/installer"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${S}/src/${GO_IMPORT}/LICENSE;md5=38f7323dc81034e5b69acc12001d6eb1"

DEPENDS = " \
    grpc \
    gtk+3 \
"

inherit go-mod pkgconfig systemd

CGO_CFLAGS += "--sysroot=${RECIPE_SYSROOT}"

REDFIELD_INSTALLER_SOURCES_URL ?= "${REDFIELD_SOURCES_URL}"
REDFIELD_INSTALLER_BRANCH ?= "${REDFIELD_BRANCH}"

GO_IMPORT = "gitlab.com/redfield/installer"
SRC_URI = " \
    git://${REDFIELD_INSTALLER_SOURCES_URL}/installer.git;protocol=${REDFIELD_SOURCES_PROTOCOL};branch=${REDFIELD_INSTALLER_BRANCH};destsuffix=${BPN}-${PV}/src/${GO_IMPORT} \
    https://redfield.dev/redfield/assets/astilectron_deps_v0.32.0.tar.gz;name=astilectron-deps \
"

SRC_URI[astilectron-deps.md5sum] = "6e2cca2278f5344e783398661b248e9d"
SRC_URI[astilectron-deps.sha256sum] = "a1411143af8e7d8c694ffd15e7b963d1c1585b8415e6b1416c30d225191854d8"

SRCREV = "${AUTOREV}"

GOBUILDMODE = 'exe'

INSANE_SKIP_${PN} = " file-rdeps already-stripped "
PRIVATE_LIBS_${PN} = " libGLESv2.so "

do_install_append() {
    DESTDIR=${D} installdir=/opt/installer make -C src/${GO_IMPORT} install

    install -d -m 0755 ${D}/opt/installer/vendor
    cp -r ${WORKDIR}/astilectron  ${D}/opt/installer/vendor/
    cp -r ${WORKDIR}/electron-linux-amd64 ${D}/opt/installer/vendor/
}

RDEPENDS_${PN} = " \
    bash \
    efibootmgr \
    lvm2 \
    parted \
    util-linux \
    libxscrnsaver \
    libasound \
    cups \
    libnss-nis \
    libpcre \
    zlib \
    libselinux \
    libsystemd \
    krb5 \
    libbsd \
    lz4 \
    libk5crypto \
    keyutils \
"
RDEPENDS_${PN}-dev = "bash make"

FILES_${PN} += " /opt"

INSANE_SKIP_${PN} += "ldflags"
INSANE_SKIP_${PN}-dev += "ldflags"
INSANE_SKIP_${PN}-staticdev += "file-rdeps"
