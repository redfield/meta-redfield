SUMMARY = "initramfs-framework module for early measurement."
DESCRIPTION = "This module will measure the dom0 rootfs image."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

PR = "r0"

inherit allarch

FILESEXTRAPATHS_prepend := "${THISDIR}/initramfs-framework:"
SRC_URI = "file://measure-redfield"

S = "${WORKDIR}"

do_install() {
    install -d ${D}/init.d
    install -m 0755 ${WORKDIR}/measure-redfield ${D}/init.d/91-measure-redfield
}

FILES_${PN} = "/init.d/91-measure-redfield"

RDEPENDS_${PN} = " \
    initramfs-framework-base \
    initramfs-module-redfield-conf \
"
