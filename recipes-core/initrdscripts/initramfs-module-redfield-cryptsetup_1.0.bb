SUMMARY = "initramfs-framework module for Redfield cryptsetup handling."
DESCRIPTION = "This module will decrypt and mount the main Redfield logical \
volume."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

PR = "r0"

inherit allarch

FILESEXTRAPATHS_prepend := "${THISDIR}/initramfs-framework:"
SRC_URI = "file://cryptsetup-redfield"

S = "${WORKDIR}"

do_install() {
    install -d ${D}/init.d
    install -m 0755 ${WORKDIR}/cryptsetup-redfield ${D}/init.d/85-cryptsetup-redfield
}

FILES_${PN} = "/init.d/85-cryptsetup-redfield"

RDEPENDS_${PN} = " \
    initramfs-framework-base \
    initramfs-module-redfield-conf \
    lvm2 \
    cryptsetup \
    udev \
"

