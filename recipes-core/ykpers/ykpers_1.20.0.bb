SUMMARY = "Yubikey Personilization Tools"

HOMEPAGE = "https://www.yubico.com/products/services-software/download/yubikey-personalization-tools"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://COPYING;md5=422217170b70a9a94c32873055b486bb"

SRC_URI = " \
    https://developers.yubico.com/yubikey-personalization/Releases/${BPN}-${PV}.tar.gz \
    file://0001-make-header-declarations-extern.patch \
"

DEPENDS += " \
    libusb \
    libyubikey \
"

inherit autotools pkgconfig

EXTRA_OECONF += " --without-json"

SRC_URI[md5sum] = "8749113ce5a0164fe2b429b61242ba0f"
SRC_URI[sha256sum] = "0ec84d0ea862f45a7d85a1a3afe5e60b8da42df211bb7d27a50f486e31a79b93"
