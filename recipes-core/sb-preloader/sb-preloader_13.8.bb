DESCRIPTION = "Linux Foundation SecureBoot Preloader"

LICENSE = "CLOSED"
LICENSE_FLAGS = "commercial_${BPN}"

SRC_URI = " \
    https://redfield.dev/redfield/assets/PreLoader.efi;md5sum=4f7a4f566781869d252a09dc84923a82 \
    https://redfield.dev/redfield/assets/HashTool.efi;md5sum=45639d23aa5f2a394b03a65fc732acf2 \
"

S = "${WORKDIR}"

do_install() {
    install -d ${D}${bindir}
    mv ${S}/PreLoader.efi ${D}${bindir}
    mv ${S}/HashTool.efi ${D}${bindir}
}

FILES_${PN} += " \
   ${bindir} \
"

INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
INHIBIT_PACKAGE_STRIP = "1"
INSANE_SKIP_${PN} = "already-stripped"
